IGTI - Atividade Prévia – Trabalho Prático

# IntegracaoContinua

1 - Configurar conta e SSH gitLab
2 - Deploy no gitlabpages
3 - Criar arquivo .gitlab-ci.yml (Resposavel pela configuração da automação do CI/CD)
4 - Configuração de Ambiente



*******************************
Comandos
- git clone https://gitlab.com/fporto.bh/integracaocontinua.git
- git add .
- git commit -m "Inserir Comentario"
- git push (envia para GitLab)


*******************************
Fontes: 
https://medium.com/ekode/primeiros-passos-com-git-e-gitlab-criando-seu-primeiro-projeto-89f9001614b0

https://medium.com/automa%C3%A7%C3%A3o-com-batista/fazendo-integra%C3%A7%C3%A3o-cont%C3%ADnua-dos-testes-automatizados-com-gitlab-ci-5aa20045d281